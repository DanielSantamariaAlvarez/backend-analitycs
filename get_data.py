def get_data(db, table):

    coleccion = db.collection(table)

    documentos = coleccion.get()

    datos = [doc.to_dict() for doc in documentos]
   

    return datos