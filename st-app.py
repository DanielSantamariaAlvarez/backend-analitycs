
import streamlit as st
import firebase_admin
import numpy as np
from firebase_admin import credentials, firestore
from datetime import datetime, timedelta
import pandas as pd
import get_data as gd
import matplotlib.pyplot as plt
import altair as alt


if not firebase_admin._apps:
    cred = credentials.Certificate("speed-lunch-2241a-firebase-adminsdk-9bzg2-7fee9a6b9e.json")
    firebase_admin.initialize_app(cred)
db = firestore.client()

def main():
    st.title("Data from Firestore team 13.3 kotlin") 
    tab1, tab2, tab3 = st.tabs(["Sprint 2", "Sprint 3", "Sprint 4"])
    with tab1:
        orders = gd.get_data(db, "orders")
        dicc_orders = {}
        for i in orders:
            if i["menuRef"] not in dicc_orders:
                dicc_orders[i["menuRef"]] = 1
            else:
                dicc_orders[i["menuRef"]] += 1
        dicc2 = {}
        
        for key, value in dicc_orders.items():
            doc_ref = db.collection("menus").document(key)
            doc = doc_ref.get()
            coso = doc.to_dict()
            dicc2[coso["name"]] = value
            
        dicc2 = dict(sorted(dicc2.items(), key=lambda item: item[1], reverse=True))
        df = pd.DataFrame(dicc2.items(), columns=['Menu', 'Orders'])
        st.write("Top 5 most ordered menus")
        df.head().to_csv("top5.csv")
        st.dataframe(df.head()) 
        st.line_chart(dicc2)
        # download csv
        with open('top5.csv') as f:
            st.download_button(label="Download CSV file", data=f, file_name="top5.csv", mime="text/csv")
        st.write("---")
        
        years = st.slider("Select age", 0, 40, 20)
        if st.button('Collect data'):
            with st.spinner(text='Running...'):
                clients = gd.get_data(db, "clients")
                
                list_ = []
                class Persona:
                    def __init__(self, birthday):
                        self.birthday = birthday
                for i in clients:
                    list_.append(Persona(i["birthday"]))
                    
                def obtener_edad(birthday_str):
                    fecha_nacimiento = datetime.strptime(birthday_str, "%Y-%m-%d")
                    edad = (datetime.now() - fecha_nacimiento) // timedelta(days=365.25)
                    return edad

                def contar_menores(lista_objetos, years):
                    contador = 0
                    edades = {}
                    for obj in lista_objetos:
                        try:
                            try:
                                edades[obtener_edad(obj.birthday)] += 1
                            except:
                                edades[obtener_edad(obj.birthday)] = 1
                            if obtener_edad(obj.birthday) < years:
                                contador += 1
                        except:
                            pass
                    return contador, edades

                
                num_, edades = contar_menores(list_, years)
                st.write(f"Number of clients under {years} years old")
                st.write(f"<p style='font-size:30px'>{num_}</p>", unsafe_allow_html=True)
                
                st.bar_chart(edades)
            
    with tab2:
        #st.header("Sprint 3")
        st.write("Top 10 most common allergies")   
        clientes = gd.get_data(db, "clients")
        dicc_clientes = {}
        for i in clientes:
            try:
                for j in i["allergies"]:
                    if j not in dicc_clientes:
                        dicc_clientes[j] = 1
                    else:
                        dicc_clientes[j] += 1
            except:
                pass
        dicc_clientes = dict(sorted(dicc_clientes.items(), key=lambda item: item[1], reverse=True))
        df = pd.DataFrame.from_dict(dicc_clientes, orient='index', columns=['quantity'])
        
        st.dataframe(df.head(10))
        
        fig, ax = plt.subplots()
        df.head(8).plot(kind='bar', ax=ax)
        ax.set_xlabel('Allergies')
        ax.set_ylabel('Quantity')
        ax.set_title('Quantity of allergies')
        st.pyplot(fig)
        
        
        st.write("---")
        st.write("Most popular Payment methods")   
        order = gd.get_data(db, "Order")
        dic_order = {}
        clientes_fieles = {}
        for i in order:
            try:
                dic_order[i["paymentMethod"]] += 1
            except:
                dic_order[i["paymentMethod"]] = 1
            
                try:
                    clientes_fieles[i["clientRef"]] += 1
                except:
                    try:
                        clientes_fieles[i["clientRef"]] = 1
                    except:
                        pass
                
                
        dicc_porcentaje = {}
        dicc_porcentaje["cash"] = dic_order["cash"] / len(order)
        dicc_porcentaje["card"] = dic_order["card"] / len(order)
                
        dic_order = dict(sorted(dic_order.items(), key=lambda item: item[1], reverse=True))
        df = pd.DataFrame.from_dict(dic_order, orient='index', columns=['paymentMethod'])
        df_fieles = pd.DataFrame.from_dict(clientes_fieles, orient='index', columns=["quantity"])
        st.dataframe(df)
        #st.bar_chart(dic_order)
        fig, ax = plt.subplots()
        df_order = pd.DataFrame.from_dict(dic_order, orient='index', columns=['quantity'])
        df_order.head(8).plot(kind='bar', ax=ax)
        ax.set_xlabel('Allergies')
        ax.set_ylabel('Quantity')
        ax.set_title('Quantity of allergies')
        st.pyplot(fig)
        
        #st.dataframe(df_fieles)
        
        df_porcentaje = pd.DataFrame.from_dict(dicc_porcentaje, orient='index', columns=['percentage'])
        #st.dataframe(df_porcentaje)
        etiquetas = list(dicc_porcentaje.keys())
        valores_grafica = list(dicc_porcentaje.values())

        # Crear la gráfica de torta
        fig, ax = plt.subplots()
        ax.pie(valores_grafica, labels=etiquetas, autopct='%1.1f%%', startangle=90)
        ax.axis('equal')  # Para que la gráfica sea un círculo en lugar de una elipse

        # Mostrar la gráfica en Streamlit
        st.pyplot(fig)
                    
        



main()

