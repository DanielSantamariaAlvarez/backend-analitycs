#%%

import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt

# Creamos un diccionario con valores y su cantidad
data = {'manzanas': 10, 'peras': 5, 'naranjas': 15, 'plátanos': 8}

# Convertimos el diccionario a un DataFrame de Pandas
df = pd.DataFrame.from_dict(data, orient='index', columns=['cantidad'])

# Creamos un gráfico de barras usando la herramienta de visualización de Pandas
st.bar_chart(df)

# Creamos un gráfico de barras usando la herramienta de visualización de Matplotlib
fig, ax = plt.subplots()
df.plot(kind='bar', ax=ax)
ax.set_xlabel('Frutas')
ax.set_ylabel('Cantidad')
ax.set_title('Cantidad de frutas')
st.pyplot(fig)

# %%
